package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.ValidatableResponse;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.JUnitCore;

import java.util.function.Consumer;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {

    @Steps
    public CarsAPI carsAPI;

    @Step("Get API FOR APPLE")
    @Test
    @When("he calls endpoint {string}")
    public void heCallsEndpoint(String arg0) {
        SerenityRest.given().get(arg0);
    }

    @Then("he sees the results displayed for apple")
    public void heSeesTheResultsDisplayedForApple() {
        restAssuredThat(response -> response.statusCode(200));
    }



    @Step("Get API FOR Mango")
    @Test
    @Then("he sees the results displayed for {string}")
    public void heSeesTheResultsDisplayedForMango(String arg0) {

        Assert.assertTrue(lastResponse().asString().contains(arg0));
    }


    @Step("Negative Scenario")
    @Test
    @Then("he sees the results displayed for Orange")
    public void heSeesTheResultsDisplayedForOrange() {

        Assert.assertFalse(lastResponse().asString().contains("Orange"));
    }

    @Step("Get API FOR CAR")
    @Test
    @Then("he doesnt not see the results")
    public void he_Doesn_Not_See_The_Results() {
        Assert.assertTrue(lastResponse().asString().contains("\"error\":true"));
    }
}
